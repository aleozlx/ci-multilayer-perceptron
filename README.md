Multilayer-perceptron: class project (of Computational intelligence) and improvements.

## Setup Environment
~~~sh
export PYTHONPATH="$PYTHONPATH:mlp"
~~~

## Test
~~~sh
pytest
~~~

## Run Example
~~~sh
python examples/vMLP_examples.py
python examples/semeion.v.py

# [deprecated]
python examples/threeclouds.py
python examples/wine.py
python examples/class_example.py
~~~

## Note

    This project was originally object oriented, and nicely implemented. But a
    few design details has been preventing the neural network from working properly.
    'threeclouds.py' example works even without biases, but they are really important.
    Then I fixed the biases problem for MultilayerPerceptron, but still validation
    error of the next 2 examples are stagnant at a very bad level. Compared to
    the matlab sample code and after trying to replicate the whole thing, I realized
    the followings:
        1. The MLP was unreasonably slow.
        2. The sample code was doing batch update while this project was doing 
            sequential update, which made it harder to further compare in order to debug.
        3. Initialization of weights wasn't done properly.
    The first 2 reasons are the main motivation that vMLP is developed, 
    which is a highly vectorized multilayer perceptron network to speed up the system.
    Batch update also becomes natural in this implementation and is preferred
    algorithmically and from a SIMD point of view, despite the fact that it
    doesn't speed up the convergency of the neural network in itself.
    Test cases were built so as to ensure the algorithm was correct from the get-go.
    Initialization problems were fixed and performance was also tuned to a decent level.
    