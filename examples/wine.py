from mlp import *
with open('datafiles/wine.data') as f:
    import random
    raw_dataset = [[float(s.strip()) for s in line.split(',')] for line in f.readlines()]
    random.shuffle(raw_dataset)

def prepare(dataset):
    targets = np.array([
        [1., -1. ,-1],
        [-1., 1. ,-1],
        [-1., -1. ,1]
    ])
    # targets = np.identity(3)
    return map(lambda row: (np.array(row[1:]), targets[row[0]-1], row[0]-1), dataset)

tanh = [np.tanh, lambda x,a=0: 1.0-a*a]
# sigmoid = [lambda x: 1.0/(1.0+np.exp(-x)), lambda x,a=0: a*(1.0-a)]
N = lambda w: Neuron(tanh, w)
L = lambda nx, ny: [N(nx+1) for i in xrange(ny)]
MLP = lambda *n: MultilayerPerceptron([L(n[i], n[i+1]) for i in xrange(len(n)-1)])
# mlp = MLP(13, 4, 3); r0 = 0.1
# mlp = MLP(13, 6, 6, 4, 4, 3); r0 = 0.25
# mlp = MLP(13, 16, 16, 8, 4, 4, 3); r0 = 0.5
mlp = MLP(13, 30, 15, 8, 4, 3); r0 = 0.3

cross = [prepare(raw_dataset[i:i+16]) for i in xrange(0, 16*10, 16)]
test = prepare(raw_dataset[160:])

def run(dataset, rate=0):
    loss = 0
    for x, t, label in dataset:
        y = mlp(x)
        if np.argmax(y)!=label:
            loss += 1
        if rate:
            e = t - y
            mlp.train1(e, rate)
    return loss*100.0/len(dataset)
    
def cross_validation(rate):
    training_loss = list(); validation_loss = list()
    for v in xrange(len(cross)):
        for i in xrange(len(cross)):
            if i!=v: # training
                training_loss.append(run(cross[i], rate))
        validation_loss.append(run(cross[v]))
    print 'T', np.average(training_loss), 'V', np.average(validation_loss)

def driver():
    for i in xrange(12):
        cross_validation(r0/(1.0+i*r0))
    print 'Accuracy: %.2f%%' % np.round(run(test), 2)

driver()
