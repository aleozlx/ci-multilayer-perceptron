from mlp import *

"""Sigmoid function and its 1st order derivative"""
sigmoid = [lambda x: 1.0/(1.0+np.exp(-x)), lambda x,a=0: a*(1.0-a)]

def class_example():
    N = lambda weights: Neuron(sigmoid, np.append(np.array(weights), [0]))
    n1 = N([.1, .1]); n2 = N([.25, .7])
    n3 = N([.4, .6]); n4 = N([.5, .3])
    x = np.array([1., 1.])
    print 'output of n1: .5498 v.', n1(x)
    mlp = MultilayerPerceptron([[n1, n2], [n3, n4]])
    y = mlp(x)
    print 'output of MLP: [.6576, .6204] v.', y
    target = np.array([1., 0.])
    error = target - y
    print 'error of MLP: [.3424, -.6204] v.', error
    mlp.train1(error, .1)
    print 'deltas of MLP: [-.0104, .0005, .0771, -.1461] v.', map(lambda neuron:neuron.delta, [n1, n2, n3, n4])
    print 'new weights of n3: [.4042, .6056] v.', n3.weights
    print 'new output: [.659, .618] v.', mlp(x)
    
def class_example_driver():
    N = lambda weights: Neuron(sigmoid, np.append(np.array(weights), [0]))
    x = np.array([1., 1.]); target = np.array([1., 0.])
    mlp = MultilayerPerceptron([
        [N([.1, .1]), N([.25, .7])], 
        [N([.4, .6]), N([.5, .3])]
    ])
    for j in xrange(10):
        for i in xrange(50):
            y = mlp(x)
            error = target - y
            mlp.train1(error, 12.0)
        print i*j, 'err:', error, 'y:', y
        
class_example()
class_example_driver()
