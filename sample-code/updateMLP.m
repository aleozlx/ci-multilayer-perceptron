%-----------------------------------------------------------
% W:  Cell array containing weight matrix for each layer
% x:  training points (NxD matrix)     N >= 1
% t:  training labels (NxC matrix)     
% eta:  learning rate
% alpha: momentum rate
% actFuncAndDervName:  string indicating funtion to use
%-----------------------------------------------------------
function [W, weightChanges] = updateMLP(W, x, t, eta, actFuncAndDervName, alpha, prevWeightChanges)

actFuncAndDerv = str2func(actFuncAndDervName);
N = size(x,1);  %Number of training points that are being used for update
nL = length(W); %Number of layers
Y = cell(nL);
Yd = cell(nL);
delta = cell(nL);
weightChanges = cell(nL);

%foreward pass
[Y{1}, Yd{1}] = actFuncAndDerv( W{1}*[x ones(N,1)]' ); 
for j=2:nL
    [Y{j}, Yd{j}] = actFuncAndDerv( W{j}*[Y{j-1}; ones(1,N)] );
end

%Backward pass
delta{nL} = Yd{nL}.*( t' - Y{nL});
for j=nL-1:-1:1
   delta{j} = Yd{j}.*( W{j+1}(:,1:end-1)' * delta{j+1} ); 
end

%Weight update
if (nargin >= 7) & (length(prevWeightChanges) == nL )
    %Use momentum..
    weightChanges{1} = eta*(1/N)*(delta{1}*[x ones(N,1)]) + alpha*prevWeightChanges{1};
    W{1} = W{1} + weightChanges{1};
    for j=2:nL
       weightChanges{j} = eta*(1/N)*(delta{j}*[Y{j-1}; ones(1,N)]') + alpha*prevWeightChanges{j};
       W{j} = W{j} + weightChanges{j};
    end
else
    %No mementum as prevWeightChanges is undefined
    weightChanges{1} = eta*(1/N)*(delta{1}*[x ones(N,1)]);
    W{1} = W{1} + weightChanges{1};
    for j=2:nL
       weightChanges{j} = eta*(1/N)*(delta{j}*[Y{j-1}; ones(1,N)]');
       W{j} = W{j} + weightChanges{j};
    end    
end

end