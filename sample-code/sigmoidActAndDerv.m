function [y, yd] = sigmoidActAndDerv( V )

y = 1 ./ ( 1 + exp(-V));
yd = y .* (1 - y);

end