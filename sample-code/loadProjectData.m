%--------------------------------------------
% X: data matrix (NxD)
% N: number of points in X
% D: dimensionality of data
% C: number of clusters
% T: data labels (NxC) - "one-hot encoded"
% dataset: raw data read from file
%--------------------------------------------
function [X,N,D,C,T,dataset] = loadProjectData(idx)

if idx == 1
    dataset = load('threeclouds.data');
    X = dataset(:,2:end);
    [N,D] = size(X);
    C = 3;
    T = zeros(N,C);
    for j=1:N
        T(j,dataset(j,1)) = 1;  %Convert to one-hot encoding for labels
    end
elseif idx == 2
    dataset = load('wine.data');
    X = dataset(:,2:end);
    [N,D] = size(X);
    C = 3;
    T = zeros(N,C);
    for j=1:N
        T(j,dataset(j,1)) = 1;  %Convert to one-hot encoding for labels
    end
else
    dataset = load('semeion.data');
    X = dataset(:,1:end-10);
    [N,D] = size(X);
    C = 10;
    T = dataset(:,end-9:end); %One-hot encoding (from the data file)
end

end