clear;

%--------------------------------
% Load data
%--------------------------------
[X,N,D,C,T,dataset] = loadProjectData(1);  % 1 = threeclouds, 2 = wine, 3 = semeion
stdev = std(X)
pause(3.0);

%--------------------------------
% Initialize network
%--------------------------------
[W, totalParameters] = initMLP([12 C], D);
actFuncAndDervName = 'sigmoidActAndDerv';
%actFuncAndDervName = 'softSignActAndDerv';
fprintf('Total network parameters: %d\n', totalParameters);

%--------------------------------------------
% Randomize data,  
% define training, validation, and test sets
%--------------------------------------------
sIdx = randperm(N);
X = X(sIdx,:);
T = T(sIdx,:);

trIdx = 1:ceil(N*0.6);                % 60%
valIdx = (trIdx(end)+1):ceil(N*0.8);  % 20%
testIdx = (valIdx(end)+1):N;          % 20%

%--------------------------------
% Normalize data
%--------------------------------
X = (X - repmat(mean(X),N,1))./repmat(std(X),N,1);

%--------------------------------
% Training 
%--------------------------------
maxIterations = 5000;
sqerrorTrain = [];
sqerrorVal = [];
weightChanges = cell(0);  %Empty to start

for j = 1:maxIterations 
    
    %Update using training set (batch)
    [W, weightChanges] = updateMLP(W,X(trIdx,:),T(trIdx,:),0.1, actFuncAndDervName, 0.2, weightChanges);
    
    %Evaluate on training and validation sets
    [na,e] = evaluateMLP(W, X(trIdx,:), T(trIdx,:), actFuncAndDervName);
    sqerrorTrain(j,1) = sum(sum( e.^2 ));
    
    [na,e] = evaluateMLP(W, X(valIdx,:), T(valIdx,:), actFuncAndDervName);
    sqerrorVal(j,1) = sum(sum( e.^2 )); 
    
    %Stop ?
    if( j > 250 & sqerrorVal(j,1) >= sqerrorVal(j-100,1) )
        break;  % Error on validation is stagnant or increasing, stop...
    end
    
    if( mod(j,100) == 99 )
        fprintf('Iteration %d:   avgSqerrorTrain:  %4f    avgSqerrorVal:  %.4f\n', j, ...
                                sqerrorTrain(j,1)./length(trIdx), sqerrorVal(j,1)./length(valIdx) );
        pause(0.1);
    end
end

%-----------------------------------------------------
% Plot error vs. epoch on training and validation sets
%-----------------------------------------------------
figure;
plot(sqerrorTrain ./ length(trIdx),'b');
hold on;
plot(sqerrorVal ./ length(valIdx),'r');
legend('training set', 'validation set');
title('Error vs. epoch');

%-----------------------------------------------
% Plot decision surface if D == 2 (threeclouds)
%-----------------------------------------------
if( D == 2 )
    xx = -5:0.05:5;
    yy = -5:0.05:5;
    [Xp,Yp] = meshgrid(xx,yy);
    Xp = Xp(:);  Yp = Yp(:);
    [res,na] = evaluateMLP(W, [Xp Yp], [], actFuncAndDervName);
    [na,idx] = max(res',[],2);
    figure;
    scatter(Xp,Yp,20,idx,'filled');
    hold on;
    colorList = { 'yellow', 'black', 'cyan' };
    for c=1:C
        plot(X(T(:,c)>0,1), X(T(:,c)>0,2), '.', 'color', colorList{mod(c-1,3)+1});
    end     
end

%--------------------------------------
% Classification accuracy on
%  training, validation, and test sets
%--------------------------------------
[res,na] = evaluateMLP(W, X, [], actFuncAndDervName);
[na,pred] = max(res',[],2);
[na,gT] = max(T,[],2);

nC = sum( pred == gT );   % Number correct on entire data set
nC_tr = sum( pred(trIdx) == gT(trIdx) );  % On training set
nC_val = sum( pred(valIdx) == gT(valIdx) );  % On validation set
nC_test = sum( pred(testIdx) == gT(testIdx) ); % On test set

ca_all = nC / N;
ca_tr = nC_tr / length(trIdx);
ca_val = nC_val / length(valIdx);
ca_test = nC_test / length(testIdx);

fprintf('Classification accuracy:  train - %.2f   validation - %.2f  test - %.2f  all - %.2f\n', ...
            ca_tr, ca_val, ca_test, ca_all);

%----------------------------------
% View some semeion data points...
%   - missclassifications
%----------------------------------
if D == 256
    figure;
    for n=1:N
        if pred(n) ~= gT(n)
            imshow(reshape(X(n,1:256),16,16)');
            title(sprintf('pred: %d   gT: %d\n', pred(n)-1, gT(n)-1));
            pause;
        end
    end
end



