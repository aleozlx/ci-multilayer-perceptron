function [y, yd] = softSignActAndDerv( V )

y = V ./ (1 + abs(V));
yd = 1 ./ ((1 + abs(V)).^2);

end