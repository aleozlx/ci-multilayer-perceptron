%-----------------------------------------------------------
% W:  Cell array containing weight matrix for each layer
% x:  training points (NxD matrix)     N >= 1
% t:  training labels (NxC matrix)  (optional)     
% actFuncAndDervName:  string indicating funtion to use
%
% y:  NxC matrix of network output(s)
% e:  t - y (if t is provided)
%-----------------------------------------------------------
function [y, e] = evaluateMLP(W, x, t, actFuncAndDervName)

actFuncAndDerv = str2func(actFuncAndDervName);
N = size(x,1);  %Number of points being evaluated
nL = length(W);
Y = cell(nL);

%foreward pass
Y{1} = actFuncAndDerv( W{1}*[x ones(N,1)]' ); 
for j=2:nL
    Y{j} = actFuncAndDerv( W{j}*[Y{j-1}; ones(1,N)] );
end

%output
y = Y{nL};
if length(t) > 0
    e = (t' - Y{nL});
else
    e = [];
end

end