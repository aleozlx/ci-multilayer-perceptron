function [W, totalParameters] = initMLP(layerNeurons, D)

nL = length(layerNeurons);
W = cell(nL,1);

W{1} = (2*rand(layerNeurons(1),D+1) - 1)./sqrt(D+1); 
totalParameters = numel(W{1});
for j=2:nL
    W{j} = (2*rand(layerNeurons(j),layerNeurons(j-1)+1) - 1)./sqrt(layerNeurons(j-1)+1);
    totalParameters = totalParameters + numel(W{j});
end

end