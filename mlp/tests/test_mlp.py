import unittest, itertools
import numpy as np
import numpy.testing as npt
import scipy.special
from mlp import Neuron, MultilayerPerceptron

"""Sigmoid function and its 1st order derivative"""
sigmoid = [scipy.special.expit, lambda x,y=0: y*(1.0-y)]

def MLP():
    N = lambda weights: Neuron(sigmoid, np.append(np.array(weights), [0]))
    return MultilayerPerceptron([
        [N([.1, .1]), N([.25, .7])], 
        [N([.4, .6]), N([.5, .3])]
    ])

class TestClassExample(unittest.TestCase):
    def test_neuron_evaluate(self):
        n1 = Neuron(sigmoid, np.array([.1, .1, 0]))
        npt.assert_allclose(n1(np.array([1., 1.])), .5498, atol=1e-4)
        
    def test_mlp_forward_pass(self):
        npt.assert_allclose(MLP()(np.array([1., 1.])), np.array([.6576, .6204]), atol=1e-4)
        
    def test_mlp_train1_delta(self):
        mlp = MLP()
        error = np.array([1., 0.]) - mlp(np.array([1., 1.]))
        mlp.train1(error, .1)
        deltas = map(lambda neuron:neuron.delta, itertools.chain(*mlp.layers))
        npt.assert_allclose(deltas, np.array([-.0104, .0005, .0771, -.1461]), atol=1e-4)
        
    def test_mlp_update_weights(self):
        mlp = MLP()
        error = np.array([1., 0.]) - mlp(np.array([1., 1.]))
        mlp.train1(error, .1)
        new_weights = np.array(map(lambda neuron:neuron.weights, itertools.chain(*mlp.layers)))
        ans_weights = np.array([
            [9.896e-02, 9.896e-02, -1.045e-03],
            [2.500e-01, 7.000e-01, 4.879e-05],
            [4.042e-01, 6.056e-01, 7.710e-03],
            [4.920e-01, 2.895e-01, -1.461e-02]
        ])
        npt.assert_allclose(new_weights, ans_weights, rtol = 1e-03)
        npt.assert_allclose(mlp(np.array([1., 0.])), np.array([0.6365, 0.6002]), atol = 1e-04)

    def test_mlp_iters(self):
        mlp = MLP()
        x = np.array([1., 1.]); target = np.array([1., 0.])
        ans = np.array([
            [0.65759674, 0.62040122],
            [0.66068681, 0.6140226],
            [0.66372407, 0.60763826],
            [0.6667098, 0.60125339],
            [0.66964523, 0.59487314],
            [0.67253157, 0.58850255]
        ])
        
        for i in xrange(ans.shape[0]):
            y = mlp(x)
            npt.assert_allclose(y, ans[i], atol = 1e-05)
            error = target - y
            mlp.train1(error, 0.1)
