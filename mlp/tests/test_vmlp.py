import unittest
import numpy as np
import numpy.testing as npt
import scipy.special
from mlp import vMLP, NeuronLayer

"""Sigmoid function and its 1st order derivative"""
sigmoid = [scipy.special.expit, lambda x,y=0: y*(1.0-y)]

def vmlp():
    return vMLP([
        NeuronLayer(sigmoid, np.array([
            [.1, .1, 0], [.25, .7, 0]
        ])),
        NeuronLayer(sigmoid, np.array([
            [.4, .6, 0], [.5, .3, 0]
        ]))
    ])

class TestClassExample(unittest.TestCase):
    def test_layer_evaluate(self):
        l1 = NeuronLayer(sigmoid, np.array([
            [.1, .1, 0], [.25, .7, 0]
        ]))
        npt.assert_allclose(
            l1(np.array([[1., 1., 1], [1., 0., 1]])),
            np.array([[.5498, .7211], [.5250, .5622]]),
        atol=1e-4)

    def test_mlp_forward_pass(self):
        npt.assert_allclose(vmlp()(np.array([
            [1., 1., 1], [1., 0., 1], [0., 0., 1], [0., 1., 1]
        ])), np.array([
            [.6576, .6204], [.6335, .6061], [.6225, .5987], [.6481, .6137]
        ]), atol=1e-4)
        
    def test_mlp_train1_delta(self):
        mlp = vmlp()
        mlp(np.array([[1., 1., 1], [1., 0., 1]]))
        mlp.train(np.array([[1., 0.], [0., 1.]]), 0.1)
        ans = [
            np.array([[-.0104, .0005], [-.0029, -.0148]]),
            np.array([[.0771, -.1461], [-.1471, .0940]])
        ]
        for layer, ans_delta in zip(mlp.layers, ans):
            npt.assert_allclose(layer.delta, ans_delta, atol=1e-4)
        
    def test_mlp_update_weights(self):
        mlp = vmlp()
        mlp(np.array([[1., 1., 1], [1., 0., 1]]))
        mlp.train(np.array([[1., 0.], [0., 1.]]), 0.1)
        ans = [
            np.array([[  9.93301627e-02,   2.49285459e-01],
                 [  9.94775582e-02,   7.00024394e-01],
                 [ -6.69837321e-04,  -7.14541414e-04]]),
            np.array([[ 0.39825869,  0.49845136],
                 [ 0.59864539,  0.29737497],
                 [-0.00349942, -0.00260404]])
        ]
        for layer, ans_weights in zip(mlp.layers, ans):
            npt.assert_allclose(layer.weights, ans_weights, atol=1e-4)
